'use strict';
let config = require('config');
let neetFee = config.get('neet').paytxfee;
exports.commands = ['tiphelp'];
exports.tiphelp = {
  usage: '<subcommand>',
  description: 'This commands has been changed to currency specific commands!',
  process: function(bot, message) {
    message.author.send(
      '__**:bank: Coins :bank:**__\n' +
      '  **Neetcoin (NEET) Tipper**\n    Transaction Fees: **' + neetFee + '**\n' +
      '__**Commands**__\n' +
      '  **$$** : Displays This Message\n' +
      '  **$$ balance** : get your balance\n' +
      '  **$$ deposit** : get address for your deposits\n' +
      '  **$$ withdraw <ADDRESS> <AMOUNT>** : withdraw coins to specified address\n' +
      '  **$$ <@user> <amount>** :mention a user with @ and then the amount to tip them\n' +
      '  **$$ private <user> <amount>** : put private before Mentioning a user to tip them privately\n' +
      '**<> : Replace carrot <> symbole with appropriate value.**\n'
    );
  }
};
