'use strict';

const bitcoin = require('bitcoin');
let BigNumber = require('bignumber.js');

let Regex = require('regex'),
  config = require('config'),
  spamchannels = config.get('moderation').botspamchannels;
let walletConfig = config.get('neet').config;
let paytxfee = config.get('neet').paytxfee;
const neet = new bitcoin.Client(walletConfig);

exports.commands = ['tipneet'];
exports.tipneet = {
  usage: '<subcommand>',
  description: '__**Neetcoin (NEET) Tipper**__\n \
                Transaction Fees: **' + paytxfee + '**\n \
                **$$** : Displays This Message\n \
                **$$ balance(bal)** : get your balance\n \
                **$$ deposit(dep)** : get address for your deposits\n \
                **$$ withdraw(wd) <ADDRESS> <AMOUNT>** : withdraw coins to specified address\n \
                **$$ public(pub) <@user> <amount>** :mention a user with @ and then the amount to tip them\n \
                **$$ <@user> <amount>** : put private before Mentioning a user to tip them privately.\n \
                **$$ multi(m) <@user1> ... <@userN> <amount>** :Multi Tips!\n \
                **$$ voice(v) <amount>** :Voice Channel.\n\n \
                **$$ block(b) ** :Blocks.\n \
                has a default txfee of ' + paytxfee,
  process: async function (bot, msg, suffix) {
    let tipper = msg.author.id.replace('!', ''),
      words = msg.content
      .trim()
      .split(' ')
      .filter(function (n) {
        return n !== '';
      }),
      subcommand = words.length >= 2 ? words[1] : 'help',
      helpmsg =
      '__**Neetcoin (NEET) Tipper**__\n \
      Transaction Fees: **' + paytxfee + '**\n \
      **$$** : Displays This Message\n \
      **$$ balance(bal)** : get your balance\n \
      **$$ deposit(dep)** : get address for your deposits\n \
      **$$ withdraw(wd) <ADDRESS> <AMOUNT>** : withdraw coins to specified address\n \
      **$$ public(pub) <@user> <amount>** :mention a user with @ and then the amount to tip them\n \
      **$$ <@user> <amount>** : put private before Mentioning a user to tip them privately.\n \
      **$$ multi(m) <@user1> ... <@userN> <amount>** :Multi Tips!\n \
      **$$ voice(v) <amount>** :Voice Channel.\n \
      **$$ block(b) ** :Blocks.\n\n \
      **<> : Replace with appropriate value.**',
      channelwarning = 'Please use <#bot-spam> or DMs to talk to bots.';
    switch (subcommand) {
      case 'help':
        privateorSpamChannel(msg, channelwarning, doHelp, [helpmsg]);
        break;
      case 'balance':
        doBalance(msg, tipper);
        break;
      case 'bal':
        doBalance(msg, tipper);
        break;
      case 'deposit':
        privateorSpamChannel(msg, channelwarning, doDeposit, [tipper]);
        break;
      case 'dep':
        privateorSpamChannel(msg, channelwarning, doDeposit, [tipper]);
        break;
      case 'withdraw':
        privateorSpamChannel(msg, channelwarning, doWithdraw, [tipper, words, helpmsg]);
        break;
      case 'wd':
        privateorSpamChannel(msg, channelwarning, doWithdraw, [tipper, words, helpmsg]);
        break;
      case 'multi':
        doMultiTip(bot, msg, tipper, words, helpmsg);
        break;
      case 'm':
        doMultiTip(bot, msg, tipper, words, helpmsg);
        break;
      case 'voice':
        doVoiceTip(bot, msg, tipper, words, helpmsg);
        break;
      case 'v':
        doVoiceTip(bot, msg, tipper, words, helpmsg);
        break;
      case 'block':
        doGetBlockInfo(msg, tipper);
        break;
      case 'b':
        doGetBlockInfo(msg, tipper);
        break;
      default:
        doTip(bot, msg, tipper, words, helpmsg);
    }
  }
};

function privateorSpamChannel(message, wrongchannelmsg, fn, args) {
  if (!inPrivateorSpamChannel(message)) {
    message.reply(wrongchannelmsg);
    return;
  }
  fn.apply(null, [message, ...args]);
}

function doHelp(message, helpmsg) {
  message.author.send(helpmsg);
}

function doGetBlockInfo(message, tipper) {
  neet.getBlockCount(function (err, blocks) {
    if (err) {
      message.author.send('Error');
    } else {

      var a = blocks % 50000;

      if(a <= 2100) {
        message.reply(`BlockNo: ${blocks}, Now it's a super block.`);
      } else {
        message.reply(`BlockNo: ${blocks}, ${50000 - a} up to the super block.`);
      } 
    }
  });
}

function doBalance(message, tipper) {
  neet.getBalance(tipper, 1, function (err, balance) {
    if (err) {
      message.reply('Error getting Neetcoin (NEET) balance.');
    } else {
      message.channel.send({
        embed: {
          description: '**Neetcoin (NEET) Balance!**',
          color: 1363892,
          fields: [{
              name: '__User__',
              value: '<@' + message.author.id + '>',
              inline: true
            },
            {
              name: '__Balance__',
              value: '**' + balance.toString() + '**',
              inline: true
            }
          ]
        }
      });
    }
  });
}

function doDeposit(message, tipper) {
  getAddress(tipper, function (err, address) {
    if (err) {
      message.reply('Error getting your Neetcoin (NEET) deposit address.');
    } else {
      message.channel.send({
        embed: {
          description: '**Neetcoin (NEET) Address!**',
          color: 1363892,
          fields: [{
              name: '__User__',
              value: '<@' + message.author.id + '>',
              inline: true
            },
            {
              name: '__Address__',
              value: '**' + address + '**',
              inline: true
            }
          ]
        }
      });
    }
  });
}

function doWithdraw(message, tipper, words, helpmsg) {
  if (words.length < 4) {
    doHelp(message, helpmsg);
    return;
  }

  var address = words[2],
    amount = getValidatedAmount(words[3]);

  if (amount === null) {
    message.reply("I don't know how to withdraw that much Neetcoin (NEET)...");
    return;
  }

  neet.getBalance(tipper, 1, function (err, balance) {
    if (err) {
      message.reply('Error getting Neetcoin (NEET) balance.');
    } else {
      if (Number(amount) + Number(paytxfee) > Number(balance)) {
        message.channel.send('It is short of outstanding balance...');
        return;
      }
      neet.sendFrom(tipper, address, Number(amount), function (err, txId) {
        if (err) {
          message.reply(err.message);
        } else {
          message.channel.send({
            embed: {
              description: '**Neetcoin (NEET) Transaction Completed!**',
              color: 1363892,
              fields: [{
                  name: '__Sender__',
                  value: '<@' + message.author.id + '>',
                  inline: true
                },
                {
                  name: '__Receiver__',
                  value: '**' + address + '**\n' + addyLink(address),
                  inline: true
                },
                {
                  name: '__txid__',
                  value: '**' + txId + '**\n' + txLink(txId),
                  inline: true
                },
                {
                  name: '__Amount__',
                  value: '**' + amount.toString() + '**',
                  inline: true
                },
                {
                  name: '__Fee__',
                  value: '**' + paytxfee.toString() + '**',
                  inline: true
                }
              ]
            }
          });
        }
      });
    }
  });
}

function doTip(bot, message, tipper, words, helpmsg) {
  if (words.length < 3 || !words) {
    doHelp(message, helpmsg);
    return;
  }

  var prv = true;
  var amountOffset = 2;

  if (words.length >= 4 && (words[1] === 'public') || (words[1] === 'pub')) {
    prv = false;
    amountOffset = 3;
  }

  if (words.length >= 4 && (words[1] === 'private') || (words[1] === 'prv')) {
    prv = true;
    amountOffset = 3;
  }

  let amount = getValidatedAmount(words[amountOffset]);

  if (amount === null) {
    message.reply("I don't know how to tip that much Neetcoin (NEET)...");
    return;
  }

  neet.getBalance(tipper, 1, function (err, balance) {
    if (err) {
      message.reply('Error getting Neetcoin (NEET) balance.');
    } else {
      if (Number(amount) + Number(paytxfee) > Number(balance)) {
        message.channel.send('It is short of outstanding balance...');
        return;
      }

      if (!message.mentions.users.first()) {
        message
          .reply('Sorry, I could not find a user in your tip...');
        return;
      }
      if (message.mentions.users.first().id) {
        sendNEET(bot, message, tipper, message.mentions.users.first().id.replace('!', ''), amount, prv);
      } else {
        message.reply('Sorry, I could not find a user in your tip...');
      }
    }
  });
}

function doMultiTip(bot, message, tipper, words, helpmsg) {
  if (words.length < 4 || !words) {
    doHelp(message, helpmsg);
    return;
  }
  var amount = getValidatedAmount(words[words.length - 1]);
  if (amount === null) {
    message.reply("I don't know how to tip that much Neetcoin (NEET)...");
    return;
  }

  var users = message.mentions.users.array();
  var recipients = [];
  for (let u of users) {
    recipients.push(u.id.replace('!', ''));
  }

  if (users.length != (words.length - 3)) {
    message.reply("Duplicate users or Unknown user is specified...");
    return;
  }

  getBalancePromise(tipper, 1)
    .then(function (balance) {
      if ((Number(amount) * users.length) + Number(paytxfee) > Number(balance)) {
        message.reply('It is short of outstanding balance...');
        return;
      }
      sendManyNEET(bot, message, tipper, recipients, amount);
    })
    .catch(function (err) {
      message.reply(err.message);
    });
}

function doVoiceTip(bot, message, tipper, words, helpmsg) {

  if (words.length < 3 || !words) {
    doHelp(message, helpmsg);
    return;
  }
  var amount = getValidatedAmount(words[words.length - 1]);
  if (amount === null) {
    message.reply("I don't know how to tip that much Neetcoin (NEET)...");
    return;
  }
  
  const voiceChannels = message.guild.channels.filter(channel => {return (channel.type === 'voice');});
  var joiningToVoiceChannel = false;
  voiceChannels.forEach(function(voiceChannel) {
    voiceChannel.members.forEach(function(member) {
      if (message.author.id == member.user.id) {
        joiningToVoiceChannel = true;
        return true;
      }
    });
  });
  if (!joiningToVoiceChannel) {
    message.reply("Please connect to the voice channel!");
    return;
  }

  const members = message.member.voiceChannel.members.filter(member => {return !member.user.bot;});

  var recipients = [];
  members.forEach(function(member) {
    if (message.author.id != member.user.id) {
      recipients.push(member.user.id.replace('!', ''));
    }
  });

  if (recipients.length <= 0) {
    message.reply("There is no one in the voice channel...");
    return;
  }

  getBalancePromise(tipper, 1)
    .then(function (balance) {
      if ((Number(amount) * recipients.length) + Number(paytxfee) > Number(balance)) {
        message.reply('It is short of outstanding balance...');
        return;
      }
      sendManyNEET(bot, message, tipper, recipients, amount);
    })
    .catch(function (err) {
      message.reply(err.message);
    });
}

function sendNEET(bot, message, tipper, recipient, amount, privacyFlag) {
  getAddress(recipient.toString(), function (err, address) {
    if (err) {
      message.reply(err.message);
    } else {
      neet.sendFrom(tipper, address, Number(amount), 1, null, null, function (err, txId) {
        if (err) {
          message.reply(err.message);
        } else {
          if (privacyFlag) {
            let userProfile = message.guild.members.find('id', recipient);
            userProfile.user.send({
              embed: {
                description: '**Neetcoin (NEET) Transaction Completed!**',
                color: 1363892,
                fields: [{
                    name: '__Sender__',
                    value: 'Private Tipper',
                    inline: true
                  },
                  {
                    name: '__Receiver__',
                    value: '<@' + recipient + '>',
                    inline: true
                  },
                  {
                    name: '__txLink__',
                    value: txLink(txId),
                    inline: true
                  },
                  {
                    name: '__Amount__',
                    value: '**' + amount.toString() + '**',
                    inline: true
                  },
                  {
                    name: '__Fee__',
                    value: '**' + paytxfee.toString() + '**',
                    inline: true
                  }
                ]
              }
            });
            message.author.send({
              embed: {
                description: '**Neetcoin (NEET) Transaction Completed!**',
                color: 1363892,
                fields: [{
                    name: '__Sender__',
                    value: '<@' + message.author.id + '>',
                    inline: true
                  },
                  {
                    name: '__Receiver__',
                    value: '<@' + recipient + '>',
                    inline: true
                  },
                  {
                    name: '__txLink__',
                    value: txLink(txId),
                    inline: true
                  },
                  {
                    name: '__Amount__',
                    value: '**' + amount.toString() + '**',
                    inline: true
                  },
                  {
                    name: '__Fee__',
                    value: '**' + paytxfee.toString() + '**',
                    inline: true
                  }
                ]
              }
            });
            if (
              message.content.startsWith('!tipneet private ')
            ) {
              message.delete(1000); //Supposed to delete message
            }
          } else {
            message.channel.send({
              embed: {
                description: '**Neetcoin (NEET) Transaction Completed!**',
                color: 1363892,
                fields: [{
                    name: '__Sender__',
                    value: '<@' + message.author.id + '>',
                    inline: true
                  },
                  {
                    name: '__Receiver__',
                    value: '<@' + recipient + '>',
                    inline: true
                  },
                  {
                    name: '__txLink__',
                    value: txLink(txId),
                    inline: true
                  },
                  {
                    name: '__Amount__',
                    value: '**' + amount.toString() + '**',
                    inline: true
                  },
                  {
                    name: '__Fee__',
                    value: '**' + paytxfee.toString() + '**',
                    inline: true
                  }
                ]
              }
            });
          }
        }
      });
    }
  });
}

function getAddress(userId, cb) {
  neet.getAddressesByAccount(userId, function (err, addresses) {
    if (err) {
      cb(err);
    } else if (addresses.length > 0) {
      cb(null, addresses[0]);
    } else {
      neet.getNewAddress(userId, function (err, address) {
        if (err) {
          cb(err);
        } else {
          cb(null, address);
        }
      });
    }
  });
}

function getAddressPromise(userId) {
  return new Promise(function (resolve, reject) {
    getAddress(userId, function (err, addresses) {
      if (err != null) {
        reject(err);
        return;
      }
      resolve(addresses);
    });
  });
}

function sendManyNEET(bot, message, tipper, recipients, amount) {
  var addrs = new Object();
  var Receivers = [];
  var promises = [];
  for (var i = 0; i < recipients.length; i++) {
    Receivers.push("<@" + recipients[i] + ">");
    promises.push(getAddressPromise(recipients[i]));
  }

  Promise.all(promises)
    .then(function (results) {
      for (var i = 0; i < results.length; i++) {
        addrs[results[i]] = Number(amount);
      }
      return addrs;
    })
    .then(function (addrs) {
      return sendManyPromise(tipper, addrs, 1, null);
    })
    .then(function (txId) {
      message.channel.send({
        embed: {
          description: '**Neetcoin (NEET) Transaction Completed!**',
          color: 1363892,
          fields: [{
              name: '__Sender__',
              value: '<@' + message.author.id + '>',
              inline: true
            },
            {
              name: '__Receiver__',
              value: Receivers.join(' '),
              inline: true
            },
            {
              name: '__txLink__',
              value: txLink(txId),
              inline: true
            },
            {
              name: '__Amount__',
              value: '**' + toMultiplied(amount, recipients.length).toString() + '**',
              inline: true
            },
            {
              name: '__Fee__',
              value: '**' + paytxfee.toString() + '**',
              inline: true
            }
          ]
        }
      });
    })
    .catch(function (err) {
      message.reply(err.message);
    });
}

function getBalancePromise(fromaccount, minconf) {
  return new Promise(function (resolve, reject) {
    neet.getBalance(fromaccount, minconf, function (err, balance) {
      if (err != null) {
        reject(err);
        return;
      }
      resolve(balance);
    });
  });
}

function sendManyPromise(fromaccount, address_amount, minconf, comment) {
  return new Promise(function (resolve, reject) {
    neet.sendMany(fromaccount, address_amount, minconf, comment, function (err, txId) {
      if (err != null) {
        reject(err);
        return;
      }
      resolve(txId);
    });
  });
}

function inPrivateorSpamChannel(msg) {
  if (msg.channel.type == 'dm' || isSpam(msg)) {
    return true;
  } else {
    return false;
  }
}

function isSpam(msg) {
  return spamchannels.includes(msg.channel.id);
};

function toMultiplied(amount, num){
  return new BigNumber(amount).multipliedBy(num)
}

function getValidatedAmount(amount) {
  amount = amount.trim();
  if (amount.toLowerCase().endsWith('neet')) {
    amount = amount.substring(0, amount.length - 3);
  }
  return amount.match(/^[0-9]+(\.[0-9]+)?$/) ? amount : null;
}

function txLink(txId) {
  return 'https://insight.neetcoin.jp/tx/' + txId;
}

function addyLink(address) {
  return 'https://insight.neetcoin.jp/address/' + address;
}